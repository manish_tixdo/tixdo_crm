/**
 * Created by consultadd on 25/1/17.
 */
import React, { Component } from 'react'
import Link from 'next/link'

import {RestService} from "../services/rest-service";
import {contentHeaders} from "../components/common/headers";
import {URLS} from "../components/common/url-constants";
import {Navbar} from "../components/navbar"
import {MovieOrderList} from '../components/movieOrderList'
import {AuthService} from "../services/auth-service";
// import cookie from 'react-cookie';
import cookie from 'cookie'
import Cookies from 'js-cookie'
import {Sidebar} from '../components/sidebar'
import Head from 'next/head'

var data = [];
class Order extends Component {
    static async getInitialProps (args) {
        const {req, res} = args
        // console.log('vvvv',req,'llll',res,'cccc')
        let rest = new RestService()
        let auth = new AuthService()
        // console.log(cookie.load('token'),'pppppppppppppppppp')
        // contentHeaders['Authorization']=cookie.load('token')
        //
        // let obj={
        //     headers:contentHeaders,
        //     url: URLS.GETMOVIEORDER,
        //     data: {}
        //
        // }
        // console.log(obj.headers,"data")
        // const result = await rest.post(obj);
        // console.log(ressult.data,"data")
        // return { orders: res.data.results }

        let token
        if (process.env.DEBUG || (!req && window.location.origin === 'http://localhost:3000')) {

            console.log('debug profile')
            token = localStorage.getItem('key')
            contentHeaders['Authorization'] = 'Token'+' '+token
            console.log(token,'sdfsf')
            let obj={
                    headers:contentHeaders,
                    url: URLS.GETMOVIEORDER,
                    data: {}

                }
            console.log(obj,"head")
            const result = await rest.post(obj);
            console.log(result.data,"data")
            return { orders: result.data.results }
            // profile = JSON.stringify({
            //     username: 'lipplocal',
            //     name: 'Gerhard Preuss',
            //     photo: 'https://avatars.githubusercontent.com/u/445883?v=3',
            //     provider: 'github'
            // })
        } else if (req) {
            token = cookie.parse(req.headers.cookie || '')
            console.log(token,"coo")
            contentHeaders['Authorization'] = token.token
            console.log(token,'sdfsf')
            let obj={
                headers:contentHeaders,
                url: URLS.GETMOVIEORDER,
                data: {}

            }
            console.log(obj,"head")
            const result = await rest.post(obj);
            console.log(result.data,"data")
            return { orders: result.data.results }
            // profile = cookies.profile
        } else {
            // profile = Cookies.get('profile')
        }
        try {
            // token = JSON.parse(profile)
            const props = Component.getInitialProps ? await Component.getInitialProps({...args, token}) : {}
            return {
                ...props,
                token
            }
        } catch (_) {
            if (autoRedirect) {
                if (req) {
                    res.writeHead(302, { Location: '/' })
                } else {
                    document.location.pathname = '/'
                }
            } else {
                return Component.getInitialProps ? await Component.getInitialProps({...args}) : {}
            }
        }
    }

    constructor(props) {
        super(props)
        this.state={
            orders:[],
        }

    }

    componentDidMount () {

        this.setState({
            orders: this.props.orders
        })



    }
    refund(){
        // alert("Do you want to refund");
    }

    render() {


        return (
            <div>
                <Head>
                <meta charset="utf-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <title>Orders</title>
                <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'/>
                <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" />
                <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500" rel="stylesheet" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>

                <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
                <link rel="stylesheet" type="text/css" href="/static/css/adminLte.min.css"/>
                <link rel="stylesheet" type="text/css" href="/static/css/allSkin.min.css"/>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"/>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"/>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"/>
                <script src="static/js/app.min.js"/>
                <script src="static/js/jquery.dataTables.min.js"/>
                <script src="static/js/dataTables.bootstrap.min.js"/>
                <script src="static/js/dataTable.js"/>


            </Head>
            <div className="hold-transition skin-blue sidebar-mini">
            <div className="wrapper">


                   <Navbar/>

                    <Sidebar/>


                            <MovieOrderList MovieOrders = {this.state.orders} />


                {/*</div>*/}
                    </div>


    </div>
                </div>

        );
    }
}

export default Order

