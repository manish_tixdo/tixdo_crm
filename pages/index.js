import React, { Component } from 'react'
import fetch from 'isomorphic-fetch'
import Link from 'next/link'
import Head from 'next/head'
import Router from 'next/router'
import {AuthService} from '../services/auth-service'

class Login extends Component {
    // static async getInitialProps ({ query: { page } }) {

    // }

    handelLogin (e) {
        e.preventDefault();
        let auth = new AuthService();
        let credentials = {
            email: this.refs.username.value,
            password: this.refs.password.value
        }
        auth.login(credentials).then(response=>{
            Router.push('/movieOrder');
        })

    }

    render () {

        return (
            <div>
                <Head>
                    <title>Login</title>
                    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans:600'/>
                    <link rel="stylesheet" type="text/css" href="/static/css/index.css"/>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"/>
                </Head>
                <div className="login-wrap">
                    <div className="login-html">
                        <input id="tab-1" type="radio" name="tab" className="sign-in" checked/>
                        <label htmlFor="tab-1" className="tab">Sign In</label>
                            <div className="login-form">
                                <div className="sign-in-htm">
                                    <form  onSubmit={this.handelLogin.bind(this)}>
                                    <div className="group">
                                        <label htmlFor="user" className="label">Username</label>
                                        <input id="user" type="text" className="input" ref='username'/>
                                    </div>
                                    <div className="group">
                                        <label htmlFor="pass" className="label">Password</label>
                                        <input id="pass" type="password" className="input" data-type="password" ref='password'/>
                                    </div>
                                    <div className="group">
                                        <input id="check" type="checkbox" className="check" checked/>
                                            <label htmlFor="check"><span className="icon"></span> Keep me Signed in
                                            </label>
                                    </div>
                                    <div className="group">
                                        <input type="submit" className="button" value="Sign In"/>
                                    </div>
                                    <div className="hr"></div>
                                    <div className="foot-lnk">
                                        <a href="#forgot">Forgot Password?</a>
                                    </div>
                                    </form>
                                </div>

                            </div>
                    </div>
                    </div>
            </div>
        )
    }
}

export default Login
