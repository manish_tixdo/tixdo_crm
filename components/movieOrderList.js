/**
 * Created by consultadd on 30/1/17.
 */
import React, { PropTypes } from 'react'
import {AuthService} from '../services/auth-service'
import Router from 'next/router'
import Head from 'next/head'

export class MovieOrderList extends React.Component {
    constructor(props) {
        super(props)

    }

    render() {


        return (
            <div>
                <Head>
                    <link rel='stylesheet prefetch' href="/static/css/movieOrderList.css"/>
                </Head>
                <div className="content-wrapper">
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                <div className="box">
                    <div className="box-body">
                        <table id="example1" className="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Book Id</th>
                                    <th>Movie Name</th>
                                    <th>Theatre Name</th>
                                    <th>Tickets</th>
                                    <th>Ticket Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                this.props.MovieOrders.map((order,i)=>(
                                    <tr key={i}>
                                        <td>{order.book_id}</td>
                                        <td>{order.movie_name}</td>
                                        <td>{order.theatre_name}</td>
                                        <td>{order.tickets}</td>
                                        <td>Rs.{order.ticket_amount}</td>
                                    </tr>
                                    )
                                )
                            }
                            </tbody>
                            </table>
                        </div>
                    </div>
                            </div>
                        </div>
                    </section>
                    </div>
                <style jsx>{`
                .table-head{
                font-size: 14px;
                font-weight: 600;
                letter-spacing: 0.5px;
                color: #444;
                }

      `}</style>
            </div>
        )
    }
}

export  default  MovieOrderList
