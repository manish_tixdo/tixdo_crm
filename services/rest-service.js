import {contentHeaders} from "../components/common/headers";
import axios from 'axios';
import Main from "../pages/index";


export class RestService {

    constructor() {

    }

    get(url) {
        var config = {
            headers: url.headers
        }
        console.log(config,"config")
        return axios.get(url.url, config)
            .then(this.checkStatus)
            .catch(this.handleErrorObservable);
    }

    post_promise(url,data) {
        var config = {
            headers: url.headers
        };
        return axios.post(url.url,data, config)
            .then(this.checkStatus)
            .catch(this.handleErrorObservable);


    }
    post(url) {
        var config = {
            headers: url.headers
        };

        return axios.post(url.url,url.data,config)
            .then(this.checkStatus)
            .catch(this.handleErrorObservable);
}




    checkStatus(response) {
        console.log("response", response)
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            console.log(error, 'ERROR')
            error.response = response
            throw error
        }
    }

    getCookie(name) {

        let ca = document.cookie.split(';');
        let caLen = ca.length;
        let cookieName = name + "=";
        let c

        for (let i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s\+/g, "");
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return "";

    }


    setHeaders(){
        let auth = new AuthService()
        let key = auth.getToken();
        if( key ){

            delete contentHeaders['X-CSRFToken'];
            contentHeaders['X-CSRFToken'] = this.getCookie('csrftoken')
            let aKey = contentHeaders['Authorization'];
            if(aKey==null || aKey==undefined || aKey=="") {
                contentHeaders['Authorization'] = 'token'+key

            }
            else{
                delete contentHeaders['Authorization'];
                contentHeaders['Authorization'] = 'token'+key

            }
        }

    }
    getCookie(name) {
        // let value = "; " + document.cookie;
        // let parts = value.split("; " + name + "=");
        // if (parts.length == 2)
        //   return parts.pop().split(";").shift();

        let ca = document.cookie.split(';');
        let caLen = ca.length;
        let cookieName = name + "=";
        let c

        for (let i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s\+/g, "");
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return "";

    }


    setCookie(name, value, expireDays, path = "") {
        let d = new Date();
        d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
        let expires = "expires=" + d.toUTCString();
        document.cookie = name + "=" + value + "; " + expires + (path.length > 0 ? "; path=" + path : "");
    }


    handleError(error) {
        // In a real world app, we might send the error to remote logging infrastructure
        let errMsg = error || '500';
        let errorCode = JSON.parse(error._body).error_code;
        // log to console instead
        return Promise.reject(errorCode);
    }

    handleAnyError(error) {
        return Observable.throw(error);
    }

    handleErrorObservable(error) {
        console.log(error, '==========>')

        // In a real world app, we might send the error to remote logging infrastructure
        let errMsg = JSON.parse(error._body) || '500';
        /******** to check the minimum amount error only for error code 907 ****/
        if (errMsg == "907") {
            let err_obj = JSON.parse(error._body);
            return Observable.throw(err_obj);
        }
        /*********************************************/
        return Observable.throw(errMsg);
    }


}