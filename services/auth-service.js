/**
 * Created by consultadd on 25/1/17.
 */
import axios from 'axios';
import Main from "../pages/index";
import {URLS} from "../components/common/url-constants";
import {RestService} from './rest-service'
import {contentHeaders} from "../components/common/headers";
import Router from 'next/router'
import cookie from 'react-cookie';
// import setRawCookie from 'react-cookie';
// import cookie from 'cookie'
import Cookies from 'js-cookie'


export class AuthService {


    constructor() {}


    login(value){
        let dataObj = {
            input_value: value.email,
            password: value.password
        }
        let dataObj1 = JSON.stringify(dataObj)
        let rest = new RestService();
        let obj ={
            url: URLS.LOGIN,
            headers: contentHeaders
        }
        return rest.post_promise(obj, dataObj1).then(response=>{
            let data = response.data;
            let auth_token = data.key;
            console.log(auth_token,"key")
            // this.setCookie('Token',data.key,1,'')

            cookie.save('token', "token"+" "+auth_token, { path: '/' });
            // setRawCookie("Token "+auth_token)
            // console.log(setRawCookie("Token"+" "+auth_token),"cookieraw")
            localStorage.setItem('key',  auth_token );
            // this.setHeaders();
            return data
        })

    }

    getToken(){
        // Retrieves the user token from localStorage
        return localStorage.getItem('key')
    }



    loggedIn(){
        // Checks if there is a saved token and it's still valid
        const token = this.getToken();
        if (token){
            return true
        }
        else{
            return false
        }

    }

    getToken(){
        return localStorage.getItem('key')
    }




    setHeaders(){
        let key = localStorage.getItem('key');
        if( key ){

            delete contentHeaders['X-CSRFToken'];
            contentHeaders['X-CSRFToken'] = this.getCookie('csrftoken')
            let aKey = contentHeaders['Authorization'];
            if(aKey==null || aKey==undefined || aKey=="") {
                contentHeaders['Authorization'] = 'Token'+" "+key

            }
            else{
                delete contentHeaders['Authorization'];
                contentHeaders['Authorization'] = 'Token'+" "+key

            }
        }

    }

    checkStatus(response) {
        console.log("response", response)
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            console.log(error, 'ERROR')
            error.response = response
            throw error
        }
    }

    getCookie(name) {
        // let value = "; " + document.cookie;
        // let parts = value.split("; " + name + "=");
        // if (parts.length == 2)
        //   return parts.pop().split(";").shift();

        let ca = document.cookie.split(';');
        let caLen = ca.length;
        let cookieName = name + " ";
        let c

        for (let i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s\+/g, "");
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return c;

    }


    setCookie(name, value, expireDays, path = "") {
        let d = new Date();
        d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
        let expires = "expires=" + d.toUTCString();
        document.cookie = name + " " + value + "; " + expires + (path.length > 0 ? "; path=" + path : "");
    }

    logout(){

        localStorage.removeItem('key');

        delete contentHeaders['Authorization'];

        this.setCookie(name, "", -1);

        const rest = new RestService();
        return axios.post(URLS.AUTH_USER_LOGOUT).then();


    }


}


